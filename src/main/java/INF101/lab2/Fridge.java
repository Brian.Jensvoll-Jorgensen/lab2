package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    List<FridgeItem> itemsInFridge = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        if (itemsInFridge.size() == 0) {
            return 0;
        }
        
        for (int i = 0; i < itemsInFridge.size(); i++) {
            int numItems = i + 1;
            if (numItems == itemsInFridge.size()) {
                return numItems;
            }
        }
        return 0;
    }

    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (itemsInFridge.size() == 20) {
            return false;
        }
        
        itemsInFridge.add(item);
        return true;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (itemsInFridge.size() == 0) {
            throw new NoSuchElementException();
        }
        
        else {
            itemsInFridge.remove(item);
        }     
    }

    @Override
    public void emptyFridge() {
        itemsInFridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expired = new ArrayList<>();

        for (int i = 0; i < itemsInFridge.size(); i++) {
            FridgeItem item = itemsInFridge.get(i);

            if (item.hasExpired() == true) {
                expired.add(item);
            }
        }

        itemsInFridge.removeAll(expired);
        return expired;
    }
    
}